module.exports = {
    UNAUTHORIZED: "Unauthorized",
    FORBIDDEN: "Forbidden",
    TOKEN_EXPIRE: "Token has expired",
    TOKEN_INVALID: "Token is invalid",
}