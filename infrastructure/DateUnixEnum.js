const moment = require('moment');

module.exports = {
    CREATED_AT: moment().unix(),
}