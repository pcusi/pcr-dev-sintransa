const jwt = require('jsonwebtoken');
const StatusCodeEnum = require('../infrastructure/StatusCodeEnum');
const ResponsesEnum = require('../infrastructure/ResponsesEnum');

exports.auth = (req, res, next) => {

    let authorization = req.headers.authorization;

    if (!authorization) return res.status(StatusCodeEnum.UNAUTHORIZED).send({ message: ResponsesEnum.UNAUTHORIZED });

    let token = authorization.split(' ')[1];

    jwt.verify(token, process.env.TOKEN_SECRET, (err, affiliate) => {

        if (err) return res.status(StatusCodeEnum.FORBIDDEN).send({ message: ResponsesEnum.FORBIDDEN });

        req._id = affiliate._id;

        next();

    });

}