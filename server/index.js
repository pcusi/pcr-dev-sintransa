require('dotenv').config();
const cors = require('cors');
const express = require('express');
const mongoose = require('mongoose');
const fileUpload = require('express-fileupload');

const app = express();

/* middlewares */
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());
app.use(fileUpload());

/* routes */
const affiliateRoutes = require('../routes/AffiliateRoutes');
const fileRoutes = require('../routes/FileRoutes');
const accountRoutes = require('../routes/AccountRoutes');
const eventRoutes = require('../routes/EventRoutes');
app.use('/api/v1', [affiliateRoutes, fileRoutes, accountRoutes, eventRoutes]);

const port = 3000 || process.env.PORT;
const mongoConnection = mongoose.connect(`${process.env.MONGODB_LOCAL}/db_sintransa`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});

app.listen(port, () => {
    mongoConnection.then(() => {
        console.log(`MongoDB connected`);
    }).catch(() => {
        console.log(`MongoDB connection error`);
    });
    console.log(`Server is running on port ${port}`);
});