const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DateUnixEnum = require('../infrastructure/DateUnixEnum');

const AffiliateSchema = new Schema({
    created_at: {
        type: Number,
        default: DateUnixEnum.CREATED_AT,
    },
    dni: {
        type: String,
        min: 8,
        max: 8,
        required: true,
    },
    is_affiliate: {
        type: Boolean,
        default: true,
    },
    lastnames: {
        type: String,
        required: true,
    },
    names: {
        type: String,
        required: true,
    },
    updated_at: {
        type: Number,
        default: 0,
    }
});

module.exports = mongoose.model('Affiliate', AffiliateSchema);