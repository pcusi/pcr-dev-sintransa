const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DateUnixEnum = require('../infrastructure/DateUnixEnum');

const SessionSchema = new Schema({
    session_date: {
        type: String,
        default: DateUnixEnum.CREATED_AT,
    },
    session_user: {
        type: String,
        required: true,
    },
    session_platform: {
        type: String,
        required: true,
    },
    session_ip: {
        type: String,
        required: true,
    }
});

module.exports = mongoose.model('Session', SessionSchema);