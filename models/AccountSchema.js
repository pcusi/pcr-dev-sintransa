const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DateUnixEnum = require('../infrastructure/DateUnixEnum');

const AccountSchema = new Schema({
    created_at: {
        type: Number,
        default: DateUnixEnum.CREATED_AT,
    },
    password: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true
    },
    affiliate_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Affiliate',
    }
});

module.exports = mongoose.model('Account', AccountSchema);