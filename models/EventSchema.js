const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DateUnixEnum = require('../infrastructure/DateUnixEnum');

const EventSchema = new Schema({
    created_at: {
        type: Number,
        default: DateUnixEnum.CREATED_AT,
    },
    description: {
        type: String,
        required: true,
    },
    event_date: {
        type: Number,
        required: true,
    },
    is_early: {
        type: Boolean,
        default: true,
    },
    title: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('Event', EventSchema);