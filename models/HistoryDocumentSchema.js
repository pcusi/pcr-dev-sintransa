const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const DateUnixEnum = require('../infrastructure/DateUnixEnum');

const HistoryDocumentSchema = new Schema({
    created_at: {
        type: Number,
        default: DateUnixEnum.CREATED_AT,
    },
    file_name: {
        type: String,
        required: true,
    },
    file_data: {
        type: Buffer
    },
    affiliate_sender: {
        type: Schema.Types.ObjectId,
        ref: 'Affiliate',
    },
    affiliate_receiver: {
        type: Schema.Types.ObjectId,
        ref: 'Affiliate',
    }
});

module.exports = mongoose.model('HistoryDocument', HistoryDocumentSchema);