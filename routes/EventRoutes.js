const express = require('express');

const router = express.Router();
const eventController = require('../controllers/EventController');
const verify = require('../middlewares/auth');

router.post('/event', verify.auth, eventController.createEvent);

module.exports = router;