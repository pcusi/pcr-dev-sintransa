const express = require('express');

const router = express.Router();
const afilliateController = require('../controllers/AffiliateController');

router.post('/affiliate', afilliateController.createAffiliate);
router.get('/affiliate', afilliateController.getAffiliates);
router.get('/affiliate/:id', afilliateController.getAffiliateById);
router.patch('/affiliate/:id', afilliateController.deaffiliate);

module.exports = router;