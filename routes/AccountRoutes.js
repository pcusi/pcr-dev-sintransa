const express = require('express');
const router = express.Router();
const accountController = require('../controllers/AccountController');

router.post('/account', accountController.createAffiliateAccount);
router.post('/account/login', accountController.logIn);

module.exports = router;