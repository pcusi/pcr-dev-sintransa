const express = require('express');

const router = express.Router();
const fileController = require('../controllers/FileController');
const verify = require('../middlewares/auth');

router.post('/file/csv', fileController.readCSVFile);
router.post('/file/pdf', verify.auth, fileController.readPdfFile);
router.get('/file/record', verify.auth, fileController.getRecordDocuments);

module.exports = router;