const nodemailer = require('nodemailer');
const Styliner = require('styliner');
const fs = require('fs');
const path = require('path');
const handlebars = require('handlebars');
const styliner = new Styliner(path.resolve(__dirname, '../templates/hbs'));

const getEmailTemplate = async () => {
    let file = fs.readFileSync(path.resolve(__dirname, '../templates/affiliate_document.hbs'), 'utf8');
    let file_with_style = styliner.processHTML(file).then(file_inline => {
        return file_inline;
    });

    return file_with_style;
};

const replaceValuesFromTemplate = async (html, replacement) => {
    const template = handlebars.compile(html);

    const html_template_values_replaced = template(replacement);

    return html_template_values_replaced;
}

async function sendDocumentToAffiliate(req, file, replacement) {
    const template = await getEmailTemplate();

    const template_replacement = await replaceValuesFromTemplate(template, replacement);

    const transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        auth: {
            user: process.env.GOOGLE_USER,
            pass: process.env.GOOGLE_PASSWORD
        }
    });

    const { from, subject } = req.body;

    const email = await transporter.sendMail({
        from: from,
        to: `${replacement.email}`,
        subject: subject, // Subject line
        text: "Hello world?", // plain text body
        html: template_replacement,
        attachments: [
            {
                filename: file.name,
                content: Buffer.from(file.data, 'base64'),
            }
        ]
    });

    if (email) return 'send';

    else return 'not-send';

}

module.exports = {
    sendDocumentToAffiliate
}