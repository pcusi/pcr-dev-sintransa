const Event = require('../models/EventSchema');
const StatusCodeEnum = require('../infrastructure/StatusCodeEnum');
const ResponsesEnum = require('../infrastructure/ResponsesEnum');
const moment = require('moment');

async function createEvent(req, res) {

    if (!req._id) return res.status(StatusCodeEnum.UNAUTHORIZED).send({ message: ResponsesEnum.UNAUTHORIZED });

    let { description, event_date, title } = req.body;

    const event_date_child = new Date(event_date);

    const event_date_unix = moment(event_date_child).unix();

    let event = new Event({
        description: description,
        event_date: event_date_unix,
        title: title
    });

    const store_event = await event.save();

    if (store_event) return res.status(StatusCodeEnum.OK).send({
        status_code: StatusCodeEnum.OK,
        message: 'Event created',
    });

    return res.status(StatusCodeEnum.BAD_REQUEST).send({
        status_code: StatusCodeEnum.BAD_REQUEST,
        message: 'Event not created',
    });
}

module.exports = {
    createEvent,
}