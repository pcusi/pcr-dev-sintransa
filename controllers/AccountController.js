const Account = require('../models/AccountSchema');
const Affiliate = require('../models/AffiliateSchema');
const Session = require('../models/SessionSchema');
const bcrypt = require('bcrypt');
const StatusCodeEnum = require('../infrastructure/StatusCodeEnum');
const jwt = require('jsonwebtoken');
const os = require('os');

async function createAffiliateAccount(req, res) {
    let { username, password, affiliate_id } = req.body;

    let hash_password = await bcrypt.hash(password, 9);

    let account = new Account({
        username,
        password: hash_password,
        affiliate_id
    });

    let store_affiliate_account = await account.save();

    if (store_affiliate_account) return res.status(StatusCodeEnum.OK).send({
        status_code: StatusCodeEnum.OK,
        message: 'Account created',
    });

    return res.status(StatusCodeEnum.BAD_REQUEST).send({
        status_code: StatusCodeEnum.BAD_REQUEST,
        message: 'Error creating account',
    });
}

async function logIn(req, res) {
    let { username, password } = req.body;

    let find_account = await Account.findOne({ username: username });

    if (!find_account) return res.status(StatusCodeEnum.BAD_REQUEST).send({
        status_code: StatusCodeEnum.BAD_REQUEST,
        message: 'Account not found',
    });

    let compare_password = await bcrypt.compare(password, find_account.password);

    if (!compare_password) return res.status(StatusCodeEnum.BAD_REQUEST).send({
        status_code: StatusCodeEnum.BAD_REQUEST,
        message: 'Password incorrect',
    });

    let token = await createToken(find_account.affiliate_id);

    if (token != "") return res.status(StatusCodeEnum.OK).send({
        status_code: StatusCodeEnum.OK,
        token,
    });
}

async function createToken(affiliate_id) {
    let token = "";

    const affiliate = await Affiliate.findOne({
        _id: affiliate_id,
        is_affiliate: true
    });

    if (!affiliate) return "This member is deaffiliate";

    const store_session = await storeAffiliateSession(`${affiliate.names} ${affiliate.lastnames}`);

    const payload = {
        _id: affiliate._id,
        affiliate: affiliate,
    };

    token = jwt.sign(payload, process.env.TOKEN_SECRET, { expiresIn: process.env.TOKEN_EXPIRES });

    return token;
}

async function storeAffiliateSession(names) {
    let network = os.networkInterfaces();

    const session = new Session({
        session_user: names,
        session_platform: os.platform(),
        session_ip: network.lo0[0].address, 
    });

    let store_session = await session.save();
}

module.exports = {
    createAffiliateAccount,
    logIn
}