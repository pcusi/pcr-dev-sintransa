const Affiliate = require('../models/AffiliateSchema');
const StatusCodeEnum = require('../infrastructure/StatusCodeEnum');

async function createAffiliate(req, res) {
    let { dni, names, lastnames } = req.body;

    let affiliate = new Affiliate({
        dni: dni,
        names: names,
        lastnames: lastnames
    });

    let find_affiliate = await Affiliate.findOne({ dni: dni });

    if (find_affiliate) return res.status(StatusCodeEnum.BAD_REQUEST).send({
        status_code: StatusCodeEnum.BAD_REQUEST,
        message: 'Affiliate already exists'
    });

    let store_affiliate = await affiliate.save();

    if (store_affiliate) return res.status(StatusCodeEnum.OK).send({
        status_code: StatusCodeEnum.OK,
        message: 'Affiliate created',
    });

    return res.status(StatusCodeEnum.BAD_REQUEST).send({
        status_code: StatusCodeEnum.BAD_REQUEST,
        message: 'Affiliate not created',
    });
}

async function getAffiliates(req, res) {
    const affiliates = await Affiliate.find();

    return res.status(StatusCodeEnum.OK).send({
        status_code: StatusCodeEnum.OK,
        affiliates
    });
}

async function getAffiliateById(req, res) {
    const affiliate_id = req.params.id;
    
    const affiliate = await Affiliate.findById(affiliate_id);

    if (!affiliate) return res.status(StatusCodeEnum.BAD_REQUEST).send({
        status_code: StatusCodeEnum.BAD_REQUEST,
        message: 'Affiliate not found'
    });

    return res.status(StatusCodeEnum.OK).send({
        status_code: StatusCodeEnum.OK,
        affiliate
    });
}

async function deaffiliate(req, res) {
    const affiliate_id = req.params.id;

    const affiliate = await Affiliate.findById(affiliate_id);

    await Affiliate.findByIdAndUpdate(affiliate_id, { is_affiliate: !affiliate.is_affiliate }, { new: true });

    return res.status(StatusCodeEnum.OK).send({
        status_code: StatusCodeEnum.OK,
        message: affiliate.is_affiliate ? 'Affiliate was deaffiliate' : 'Affiliate was affiliate',
    });
}

module.exports = {
    createAffiliate,
    getAffiliates,
    getAffiliateById,
    deaffiliate
}