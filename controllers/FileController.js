const xlsx = require('xlsx');
const DateUnixEnum = require('../infrastructure/DateUnixEnum');
const StatusCodeEnum = require('../infrastructure/StatusCodeEnum');
const ResponsesEnum = require('../infrastructure/ResponsesEnum');
const Affiliate = require('../models/AffiliateSchema');
const HistoryDocument = require('../models/HistoryDocumentSchema');
const email = require('./EmailController');

function readCSVFile(req, res) {

    //if (!req._id) return res.status(StatusCodeEnum.UNAUTHORIZED).send({ message: ResponsesEnum.UNAUTHORIZED });

    const csv_file = req.files.csv_file;

    const path = `${process.cwd()}/upload/files/${csv_file.name}`;

    csv_file.mv(path, async (err) => {
        if (err) return res.status(StatusCodeEnum.INTERNAL_SERVER_ERROR).send({
            message: 'Error uploading file'
        });

        const workbook = xlsx.readFile(path);
        const sheet_name_list = workbook.SheetNames;
        const sheet_affiliates = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]).map(affiliate => {
            return {
                names: affiliate.names,
                lastnames: affiliate.lastnames,
                dni: affiliate.dni.toString(),
            }
        });

        const store_affiliate_csv = await csvFilterData(sheet_affiliates);

        return res.status(StatusCodeEnum.OK).send({
            message: store_affiliate_csv
        });
    });
}

async function csvFilterData(sheet_affiliates) {

    const affiliates = await Affiliate.find({});
    const filter_affiliates = filterDuplicates(sheet_affiliates, affiliates);
    const store_affiliates = await csvStoreData(filter_affiliates);
    let message = "";

    filter_affiliates.length === 0 ? (message = "No affiliates to add") : (message = store_affiliates);

    return message;
}

async function csvStoreData(filter_affiliates) {

    const store_affiliates = await Affiliate.insertMany(filter_affiliates);

    if (store_affiliates) return "Affiliates added successfully";

    return "Error adding affiliates";
}

function filterDuplicates(A, B) {
    const a = new Set(A.map(x => x.dni));
    const b = new Set(B.map(x => x.dni));
    return [...A.filter(x => !b.has(x.dni)), ...B.filter(x => !a.has(x.dni))]
}

function readPdfFile(req, res) {

    if (!req._id) return res.status(StatusCodeEnum.UNAUTHORIZED).send({
        message: ResponsesEnum.UNAUTHORIZED
    });

    const pdf_file = req.files.pdf_file;

    const path = `${process.cwd()}/upload/documents/${pdf_file.name}`;

    if (pdf_file.mimetype !== 'application/pdf') return res.status(StatusCodeEnum.BAD_REQUEST).send({
        message: 'File must be a PDF'
    });

    pdf_file.mv(path, async (err) => {
        if (err) return res.status(StatusCodeEnum.INTERNAL_SERVER_ERROR).send({
            message: 'Error uploading file'
        });

        const replacement_values = {
            names: 'Mi puerk, mi amor, mi love, mi cerdita, mi princesita, mi tooooooo',
            email: 'pcusir@gmail.com',
        }

        const send_email = await email.sendDocumentToAffiliate(req, pdf_file, replacement_values);

        const store_history_document = await storeHistoryDocument(req, pdf_file);

        return res.status(StatusCodeEnum.OK).send({
            status_code: StatusCodeEnum.OK,
            message: send_email,
            history_document: store_history_document
        });

    });
}

async function storeHistoryDocument(req, file) {

    let { affiliate_receiver } = req.body;

    const history_document = new HistoryDocument({
        file_name: file.name,
        file_data: Buffer.from(file.data),
        affiliate_sender: req._id,
        affiliate_receiver: affiliate_receiver,
    });

    const store_history_document = await history_document.save();

    store_history_document ? 'History document was created' : 'History document was not created';

    return;

}

async function getRecordDocuments(req, res) {

    const history_documents = await HistoryDocument.find({})
                                                   .select("-file_data")
                                                   .populate("affiliate_sender")
                                                   .populate("affiliate_receiver");

    return res.status(StatusCodeEnum.OK).send({
        records: history_documents
    });

}

module.exports = {
    readCSVFile,
    readPdfFile,
    getRecordDocuments,
}